terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.6.2"
    }
  }
}

provider "aws" {
  access_key = ""
  secret_key = ""
  region     = "us-east-1"
  # Configuration options
}
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "vpc-terraform"
  }
}
resource "aws_subnet" "subnet_1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1b" 
  tags = {
    Name = "vpc-terra-subnet"
  }
}
data "aws_vpc" "default_vpc" {
  default = true
}
resource "aws_subnet" "subnet_2" {
  vpc_id     = data.aws_vpc.default_vpc.id
  cidr_block = "172.31.96.0/20"
  availability_zone = "us-east-1b" 
  tags = {
    Name = "vpc-terra-subnet-2"
  }
}